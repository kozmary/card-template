#!/bin/bash

sizes=( 26 64 )

for size in ${sizes[@]}
do
	printf "Converting %dx%d icons...\n" "$size" "$size"
	mkdir -p png/$size
	for file in svg/${size}/*.svg
	do
		b=$(basename ${file} .svg)
		convert -background none $file -resize ${size}x${size} png/${size}/$b.png 
	done
done

